
import java.util.Scanner;



public class Years {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int yearOfBirth, addedYears, futureDate;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the year you were born");
		yearOfBirth = keyboard.nextInt();
		System.out.println("Enter how many years in the future you wish to advance");
		addedYears = keyboard.nextInt();
		
		futureDate = yearOfBirth + addedYears;
		
		System.out.println("If you were born in the year " +  yearOfBirth);
		System.out.println("After advancing " +  addedYears + " years into the future");
		System.out.println("The year will be " +  futureDate);
}
}

